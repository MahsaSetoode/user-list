import FormBox from "../UI/FormBox";
import UserItem from "./UserItem";
import styles from './UserList.module.css'
const UserList = props => {    
    
    return(
        <FormBox className={styles.users}>
            <ul >
                {/* to map that array of users to  an array of JSX elements*/}
                {props.users.map(user => 
                <UserItem
                    key={user.id}
                    id={user.id}
                    onDelete={props.onDeleteUser}
                >
                    {user.name}
                    ({user.age} years old) is submmited.
                </UserItem>
                )}
            </ul>
        </FormBox> 
    )
}

export default UserList;