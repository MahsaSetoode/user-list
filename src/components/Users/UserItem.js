// import styles from './UserItem.module.css'

const UserItem = props => {
    const deleteHandler = () => {
        // setDeleteText('(Deleted!)');
        props.onDelete(props.id);
    };
    
    return (
        <li onClick={deleteHandler}>
            {props.children}
        </li>
    )
}

export default UserItem;