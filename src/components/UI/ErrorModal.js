import Button from "./Button";
import FormBox from "./FormBox";
import styles from './ErrorModal.module.css'

const ErrorModal = props => {
    return (
        <div className={styles.backdrop} onClick={props.onConform}>
            <FormBox className={styles.modal}>
                <header className={styles.header}>
                    <h2>{props.title}</h2>
                </header>
                <div className={styles.content}>
                    <p>{props.message}</p>
                </div>
                <footer className={styles.actions}>
                    <Button onClick={props.onConform}>I Understand</Button>
                </footer>
            </FormBox>
        </div>
    );  
}

export default ErrorModal;