import styles from './FormBox.module.css'

const FormBox = props => {
    return <div className={`${styles.formBox} ${props.className}`}>
        {props.children}
    </div>
}

export default FormBox;