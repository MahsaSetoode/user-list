import './App.css';
import AddUser from './components/Users/AddUser';
import UserList from './components/Users/UserList';
import { useState } from 'react';

function App() {
  const [userInfo, setUserInfo] = useState([]);

  const addUserHandler = (userName,userAge) => {
    setUserInfo(prevUserList => {
      const updatedInfo = [...prevUserList];
      updatedInfo.unshift({ name: userName, age: userAge, id: Math.random().toString()});
      return updatedInfo;
    });
  };

  const deleteUserHandler = userId => {
    setUserInfo(prevUserList => {
      const updatedInfo = prevUserList.filter(user => user.id !== userId);
      return updatedInfo;
    });
  };

  let content = (
    <p style={{ textAlign: 'center' ,color: 'aliceblue'}}>No user is available!</p>
  );

  if (userInfo.length > 0) {
    content = (
      <UserList users={userInfo} onDeleteUser={deleteUserHandler}/>
    );
  }

  return (
    <div>
      <section>
        <AddUser onAdd={addUserHandler} />
      </section>
      <section>
        {content}
      </section>
    </div>
  );
}

export default App;
